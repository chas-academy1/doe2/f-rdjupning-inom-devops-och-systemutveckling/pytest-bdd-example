class Article:
    def __init__(self, text):
        self.text = text
        self._published = False

    def publish(self):
        self._published = True

    def unpublish(self):
        self._published = False

    @property
    def is_published(self):
        return self._published

