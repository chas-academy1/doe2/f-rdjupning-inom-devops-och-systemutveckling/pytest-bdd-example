Feature: Unpublishable Articles

    An article can be unpublished after publication

    Scenario: Unpublishing an article
        Given An article is created
        And it's published
        When it's unpublished
        Then it's state is unpublished
