Feature: Publishable Articles

    A library to handle articles with text, and a published state.

    Scenario: Creating an article doesn't publish it
        Given An article is created
        And it's not explicitly published
        Then it's state is unpublished

    
    Scenario: Explicitly publishing an article publishes it
        Given An article is created
        And it is explicitly published
        Then it's state is published
